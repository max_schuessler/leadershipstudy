﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beardAntiClip : MonoBehaviour {

    SkinnedMeshRenderer beard;
    SkinnedMeshRenderer body;
    string[] beardShapeList;
    string[] bodyShapeList;

    int[] beardToBodyIndex;

	void Start ()
    {
        beard = GetComponent<SkinnedMeshRenderer>();
        body = GameObject.Find("Body").GetComponent<SkinnedMeshRenderer>();

        beardShapeList = getBlendShapeNames(beard.gameObject);
        bodyShapeList = getBlendShapeNames(body.gameObject);

        beardToBodyIndex = new int[beardShapeList.Length];

        for (int i = 0; i < bodyShapeList.Length; i++)
        {
            for (int e = 0; e < beardShapeList.Length; e++)
            {
                if (bodyShapeList[i] == beardShapeList[e])
                {
                    beardToBodyIndex[e] = i;
                }
            }
        }
    }
	
	void Update ()
    {
        for (int e = 0; e < beardShapeList.Length; e++)
        {
            beard.SetBlendShapeWeight(e, body.GetBlendShapeWeight(beardToBodyIndex[e]));
        }
    }

    public string[] getBlendShapeNames(GameObject obj)
    {
        SkinnedMeshRenderer head = obj.GetComponent<SkinnedMeshRenderer>();
        Mesh m = head.sharedMesh;
        string[] arr;
        arr = new string[m.blendShapeCount];
        for (int i = 0; i < m.blendShapeCount; i++)
        {
            string s = m.GetBlendShapeName(i);
            print("Blend Shape: " + i + " " + s); // Blend Shape: 4 FightingLlamaStance
            arr[i] = s;
        }
        return arr;
    }
}
