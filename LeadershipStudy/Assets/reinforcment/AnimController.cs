﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using RogoDigital.Lipsync;

public class AnimController : MonoBehaviour {

	public LipSync jace;
	public LipSyncData intro, int4, int4neg, int4pos, int9, int9posNeg, int9negPos, int9neg, int9pos;
	public Animator jaceAnim;
	public bool surveyEnded, rechecking;
	public float[] intervals;
	public int currInterval;

	public string url, currCommand = "";
	WWW commandData;

	// Use this for initialization
	void Start () {
		//Uncomment these 2 lines to run the http request for commands
		commandData = new WWW(url);
		StartCoroutine(GetCommands());
		currInterval = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private IEnumerator GetCommands() {
		//Wait for 5 seconds and check for new command

		//Wait 5 seconds on top of the current intervals
		yield return new WaitForSeconds (5f);
		yield return new WaitForSeconds (rechecking ? 0f : intervals[currInterval]);

		commandData = new WWW(url);
		yield return commandData;
		string thisCommand = commandData.text;

		if (thisCommand != currCommand) {
			currCommand = thisCommand;
			DoCommand();
		}else {
			print ("Re-checking for new command");
			rechecking = true;
			StartCoroutine(GetCommands());
		}


	}

	private void DoCommand() {
		print (currCommand);
		switch (currCommand) {
		case "hello":
			print ("hello worked");
				break;
		case "intro":
			PlayIntro ();
			break;
		case "int4neutral":
			PlayInt4Neutral();
			break;
		case "int4negative":
			PlayInt4Negative();
			break;
		case "int4positive":
			PlayInt4Positive();
			break;
		case "int9neutral":
			PlayInt9Neutral();
			break;
		case "int9negative":
			PlayInt9Negative();
			break;
		case "int9positive":
			PlayInt9Positive();
			break;
		case "int9posnumnegqual":
			PlayInt9PosNumNegQual();
			break;
		case "int9negnumposqual":
			PlayInt9NegNumPosQual();
			break;
		}
		rechecking = false;
		currInterval++;
		if (currInterval > intervals.Length)
			return;
		
		StartCoroutine(GetCommands());
	}

	public void PlayIntro() {
		jace.Play(intro);
		//jaceAnim.SetTrigger("happyIdle_trigger");
	}

	public void PlayInt4Neutral() {
		jace.Play(int4);
		//jaceAnim.SetTrigger("idle_trigger");
	}

	public void PlayInt4Negative() {
		jace.Play(int4neg);
	}

	public void PlayInt4Positive() {
		jace.Play(int4pos);
	}

	public void PlayInt9Neutral() {
		jace.Play(int9);
	}

	public void PlayInt9Negative() {
		jace.Play(int9neg);
	}

	public void PlayInt9Positive() {
		jace.Play(int9pos);
	}

	public void PlayInt9PosNumNegQual() {
		jace.Play(int9posNeg);
	}

	public void PlayInt9NegNumPosQual() {
		jace.Play(int9negPos);
	}
}
